const mongoose = require("mongoose");
const miesquema = mongoose.Schema;
const esquemaProducto = new miesquema({
  //_id : miesquema.Types.ObjectId,
  id: Number,
  title: String,
  price: Number,
  category: String,
  quantity: Number,
  iva: Number,
  image: String,
  description: String,
  state: Boolean,
});
const modeloProducto = mongoose.model("inventario", esquemaProducto);
module.exports = modeloProducto;
