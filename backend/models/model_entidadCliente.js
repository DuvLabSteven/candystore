const mongoose = require("mongoose");
const miesquema = mongoose.Schema;
const esquemaCliente = new miesquema({
  //_id : miesquema.Types.ObjectId,
  nombreCliente : String,
  identificacion: String,
  tipoIdentificacion: Number,
  direccion: String,
  tipoCliente: Number,
  correoElectronico: String,
  estado: Boolean,
});
const modeloCliente = mongoose.model("entidadCliente", esquemaCliente);
module.exports = modeloCliente;