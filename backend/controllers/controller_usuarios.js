const express = require("express");
const Usuario = require("../models/model_usuarios");
const bcryptjs = require("bcryptjs");
const { validationResult } = require("express-validator");
const jwt = require("jsonwebtoken");

exports.listarUsuarios = async (req, res) => {
  Usuario.find({}, function (docs, err) {
    if (!err) {
      res.send(docs);
    } else {
      res.send(err);
    }
  });
};

exports.crearUsuario = async (req, res) => {
  //revisar si hay errores
  const errores = validationResult(req);
  if (!errores.isEmpty()) {
    return res.status(400).json({ errores: errores.array() });
  }

  const { email, password } = req.body;
  try {
    //Verificamos si el usuario ya existe
    let usuario = await Usuario.findOne({ email });
    if (usuario) {
      return res.status(400).json({ msg: "El usuario ya existe" });
    }
    // creamos nuestro Usuario
    usuario = new Usuario(req.body);
    usuario.password = await bcryptjs.hash(password, 10);
    await usuario.save();
    res.send(usuario);

    //Firmar el JWT
    const payload = {
      usuario: { id: usuario.id },
    };

    jwt.sign(
      payload,
      process.env.SECRETA,
      {
        expiresIn: 3600, //1 hora
      },
      (error, token) => {
        if (error) throw error;

        //Mensaje de confirmación
        res.json({ token });
      }
    );
  } catch (error) {
    console.log(error);
    res.status(500).send("hay un error al recibir los datos");
  }
};

exports.obtenerUsuario = async (req, res) => {
  try {
    let usuario = await Usuario.findById(req.params.id);
    if (!usuario) {
      res.status(404).json({ msg: "el Usuario no existe" });
    }
    res.json(usuario);
  } catch (error) {
    console.log(error);
    res.status(500).send("hay un error al recibir los datos");
  }
};

exports.eliminarUsuario = async (req, res) => {
  try {
    let usuario = await Usuario.findById(req.params.id);
    if (!usuario) {
      res.status(404).json({ msg: "El usuario no existe" });
    }
    await Usuario.findByIdAndRemove({ _id: req.params.id });
    res.json({ msg: "Usuario eliminado con exito" });
  } catch (error) {
    console.log(error);
    res.status(500).send("hay un error al recibir los datos");
  }
};

exports.actualizarUsuario = async (req, res) => {
  try {
    const { name, email, password } = req.body;
    let usuario = await Usuario.findById(req.params.id);
    if (!usuario) {
      res.status(404).json({ msg: "el Usuario no existe" });
    }
    usuario.name = name;
    usuario.email = email;
    usuario.password = await bcryptjs.hash(password, 10);

    usuario = await Usuario.findOneAndUpdate({ _id: req.params.id }, usuario, {
      new: true,
    });
    const payload = {
      usuario: { id: usuario.id },
    };

    jwt.sign(
      payload,
      process.env.SECRETA,
      {
        expiresIn: 3600, //1 hora
      },
      (error, token) => {
        if (error) throw error;

        //Mensaje de confirmación
        res.json({ token });
      }
    );
    res.json(usuario);
  } catch (error) {
    console.log(error);
    res.status(500).send("hay un error al recibir los datos");
  }
};
