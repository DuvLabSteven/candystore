const express = require("express");

const router = express.Router();
const modeloProducto = require("../models/model_inventario");
// incluyendo comentarios

/**
 * Metodo para listar registros de la colección http://localhost:8000/api/inventario/listar
 * */
exports.listarProductos = (req, res) => {
  modeloProducto.find({}, function (docs, err) {
    if (!err) {
      res.send(docs);
    } else {
      res.send(err);
    }
  });
};

/**
 * Metodo para cargar un producto http://localhost:8000/api/inventario/agregar
 */
exports.crearProducto = (req, res) => {
  const nuevoProducto = new modeloProducto({
    id: req.body.id,
    title: req.body.title,
    price: req.body.price,
    quantity: req.body.quantity,
    category: req.body.category,
    iva: req.body.iva,
    image: req.body.image,
    description: req.body.description,
    state: req.body.state,
  });
  nuevoProducto.save(function (err) {
    if (!err) {
      res.send("El producto fue agregado exitosamente al inventario!");
    } else {
      res.send(err);
    }
  });
};

/**
 * Metodo para cargar un producto http://localhost:8000/api/inventario/cargar/1
 */
exports.mostrarProducto = (req, res) => {
  const { id } = req.params;
  modeloProducto
    .findById(id)
    .then((data) => res.json(data))
    .catch((error) => res.json({ mensaje: error }));
};

/**
 * Metodo para editar un producto
 */
exports.actualizarProducto = async (req, res) => {
  // Haciendo búsqueda por referencia
  try {
    const {
      id,
      title,
      price,
      category,
      quantity,
      iva,
      image,
      description,
      state,
    } = req.body;
    let producto = await modeloProducto.findById(req.params.id);
    if (!producto) {
      res.status(404).json({ msg: "el producto no existe" });
    }
    producto.id = id;
    producto.title = title;
    producto.price = price;
    producto.category = category;
    producto.quantity = quantity;
    producto.iva = iva;
    producto.image = image;
    producto.description = description;
    producto.state = state;

    producto = await modeloProducto.findOneAndUpdate(
      { _id: req.params.id },
      producto,
      { new: true }
    );
    res.json(producto);
  } catch (error) {
    console.log(error);
    res.status(500).send("hay un error al recibir los datos");
  }
};

/**
 * Metodo para agregar productos
 */
exports.eliminarProducto = async (req, res) => {
  try {
    let producto = await modeloProducto.findById(req.params.id);
    if (!producto) {
      res.status(404).json({ msg: "el producto no existe" });
    }
    await modeloProducto.findByIdAndRemove({ _id: req.params.id });
    res.json({ msg: "producto eliminado con exito" });
  } catch (error) {
    console.log(error);
    res.status(500).send("hay un error al recibir los datos");
  }
};
