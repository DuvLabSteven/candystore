const express = require("express");
const router = express.Router();
const modeloCliente = require("../models/model_entidadCliente");

/**
 * Metodo para listar registros de la colección entidadCliente
 */
exports.listarClientes = (req, res) => {
  modeloCliente.find({}, function (docs, err) {
    if (!err) {
      res.send(docs);
    } else {
      res.send(err);
    }
  });
};

/*
 * Metodo para agregar clientes http://localhost:8000/api/entidadCliente/agregarCliente
 */
exports.crearClientes = (req, res) => {
  const nuevoCliente = new modeloCliente({
    nombreCliente: req.body.nombreCliente,
    identificación: req.body.identificacion,
    tipoIdentificacion: req.body.tipoIdentificacion,
    direccion: req.body.direccion,
    tipoCliente: req.body.tipoCliente,
    correoElectronico: req.body.correoElectronico,
    estado: req.body.estado,
  });
  nuevoCliente.save(function (err) {
    if (!err) {
      res.send(
        "El cliente fue agregado exitosamente a la Colección entidadClientes!"
      );
    } else {
      res.send(err);
    }
  });
};

/**
 * Metodo para cargar un producto http://localhost:8000/api/entidadCliente/cargarCliente/:id
 */
exports.mostrarCliente = (req, res) => {
  const { id } = req.params;
  modeloCliente
    .findById(id)
    .then((data) => res.json(data))
    .catch((error) => res.json({ mensaje: error }));
};

/*
 *  * Metodo para editar un producto
 */
exports.actualizarCliente = (req, res) => {
  // Haciendo búsqueda por referencia
  modeloCliente.findOneAndUpdate(
    { identificacion: req.params.identificacion },
    {
      nombreCliente: req.body.nombreCliente,
      identificacion: req.body.identificacion,
      tipoIdentificacion: req.body.tipoIdentificacion,
      direccion: req.body.direccion,
      tipoCliente: req.body.tipoCliente,
      correoElectronico: req.body.correoElectronico,
      estado: req.body.estado,
    },
    (err) => {
      if (!err) {
        res.send("El cliente se actualizó exitosamente!!!");
      } else {
        res.send(err);
      }
    }
  );
};

exports.eliminarCliente = async (req, res) => {
  try {
    let cliente = await modeloCliente.findById(req.params.id);
    if (!cliente) {
      res.status(404).json({ msg: "el cliente no existe" });
    }
    await modeloCliente.findByIdAndRemove({ _id: req.params.id });
    res.json({ msg: "cliente eliminado con exito" });
  } catch (error) {
    console.log(error);
    res.status(500).send("hay un error al recibir los datos");
  }
};
