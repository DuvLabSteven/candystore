//Rutas para crear usuarios

const express = require("express");
const router = express.Router();
const controladorUsuarios = require("../controllers/controller_usuarios");
const { check } = require("express-validator");

router.post(
  "/",
  [
    check("name", "El nombre es obligatorio").not().isEmpty(),
    check("email", "Agrega un email válido").isEmail(),
    check("password", "El password debe ser mínimo de 6 caracteres").isLength({
      min: 6,
    }),
  ],
  controladorUsuarios.crearUsuario
);

router.put("/:id", controladorUsuarios.actualizarUsuario);
router.get("/", controladorUsuarios.listarUsuarios);
router.delete("/:id", controladorUsuarios.eliminarUsuario);
module.exports = router;
