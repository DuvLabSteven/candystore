import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import ContentHeader from "../../componentes/ContentHeader";
import Footer from "../../componentes/Footer";
import Navbar from "../../componentes/Navbar";
import SidebarContainer from "../../componentes/SidebarContainer";
import APIInvoke from "../../utils/APIInvoke";
import swal from "sweetalert";

const AdminClientes = () => {
    const [clientes, setClientes] = useState([]);

    const cargarClientes = async () => {
        const response = await APIInvoke.invokeGET("/api/entidadCliente");

        setClientes(response);
    };

    useEffect(() => {
        cargarClientes();
    }, []);

    //Eliminar cliente
    const eliminarCliente = async (e, idCliente) => {
        e.preventDefault();
        const response = await APIInvoke.invokeDELETE(`/api/entidadCliente/${idCliente}`);

        if (response.msg === 'cliente eliminado con exito') {
            const msg = "El cliente fue borrado correctamente.";
            swal({
                title: 'Información',
                text: msg,
                icon: 'success',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-primary',
                        closeModal: true
                    }
                }
            });
            cargarClientes();
        } else {
            const msg = "El cliente no fue borrado correctamente.";
            swal({
                title: 'Error',
                text: msg,
                icon: 'error',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-danger',
                        closeModal: true
                    }
                }
            });
        }

    }

    return (
        <div className="wrapper">
            <Navbar></Navbar>
            <SidebarContainer></SidebarContainer>
            <div className="content-wrapper">
                <ContentHeader
                    titulo={"Listado de Clientes"}
                    breadCrumb1={"Inicio"}
                    breadCrumb2={"Clientes"}
                    ruta1={"/home"}
                />

                <section className="content">
                    <div className="card">
                        <div className="card-header">
                            <h3 className="card-title"><Link to={"/crear-cliente"} className="btn btn-block btn-primary btn-sm">Crear Cliente</Link></h3>
                            <div className="card-tools">
                                <button type="button" className="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                    <i className="fas fa-minus" />
                                </button>
                                <button type="button" className="btn btn-tool" data-card-widget="remove" title="Remove">
                                    <i className="fas fa-times" />
                                </button>
                            </div>
                        </div>
                        <div className="card-body">
                            <table className="table table-bordered">
                                <thead>
                                    <tr>
                                        <th style={{ width: "5%" }}>Id</th>
                                        <th style={{ width: "10%" }}>Nombre</th>
                                        <th style={{ width: "10%" }}>Identificacion</th>
                                        <th style={{ width: "10%" }}>Tipo de identificacion</th>
                                        <th style={{ width: "10%" }}>Direccion</th>
                                        <th style={{ width: "10%" }}>Tipo cliente</th>
                                        <th style={{ width: "10%" }}>Corre electronico</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        clientes.map((item) => (
                                            <tr key={item._id}>
                                                <td>{item._id}</td>
                                                <td>{item.nombreCliente}</td>
                                                <td>{item.identificacion}</td>
                                                <td>{item.tipoIdentificacion}</td>
                                                <td>{item.direccion}</td>
                                                <td>{item.tipoCliente}</td>
                                                <td>{item.correoElectronico}</td>
                                                <td>{item.estado}</td>
                                                <td>
                                                    <Link to={`/editar-cliente/${item._id}@${item.nombre}@${item.edad}@${item.favoritos}@${item.premium}`} className="btn btn-sm btn-primary">Editar</Link>&nbsp;&nbsp;

                                                    <button onClick={(e) => eliminarCliente(e, item._id)} className="btn btn-sm btn-danger">Borrar</button>

                                                </td>
                                            </tr>
                                        ))}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
            <Footer></Footer>
        </div>
    );
};

export default AdminClientes;
